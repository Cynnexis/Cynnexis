# Valentin Berger

[![](https://img.shields.io/badge/LinkedIn-Valentin_Berger-0A66C2?style=flat&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/valentinberger/) ![](https://img.shields.io/badge/Skype-Valentin.berger-00ACEE?style=flat&logo=skype&logoColor=white) [![](https://img.shields.io/badge/GitHub-Cynnexis-7D007B?style=flat&logo=github&logoColor=white)](https://github.com/Cynnexis) [![](https://img.shields.io/badge/GitLab-Cynnexis-FC6D26?style=flat&logo=gitlab&logoColor=white)](https://gitlab.com/Cynnexis)

![Cover card](res/img/cover.png)

![Timeline](res/img/timeline.png)

Icons from [CoreUI](https://icons.coreui.io/icons) licensed under the [CoreUI Icons Free License][coreui-license]. See [`res/licenses/`](res/licenses) to read the licenses.

[coreui-license]: https://github.com/coreui/coreui-icons/blob/master/LICENSE
